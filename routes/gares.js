var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID

MongoClient.connect('mongodb://localhost:27017/TodoList', { useNewUrlParser: true }, function (err, client) {
  if (err) throw err

  let db = client.db('examD2D13')
  /* GET home page. */

  router.get('/', function(req, res, next) {
    db.collection('gares').find().toArray(function (err, result) {
      res.render('gares', { title: 'SNCF', gares: result });


    })
  });

 //supprimer une gare
  router.use(function(req, res, next) {
   res.setHeader('Content-Type', 'application/json');
  next()
});

router.delete('/:id', function(req, res, next) {
gareId = req.params.id,
 db.collection('gares').deleteOne({_id : ObjectId(gareId)}, function(error, result) {
   if(err) return next(err);
   return res.json(result) ;

 })
});


//ajouter une gare
    router.post('/', function(req, res, next) {
    db.collection('gares').insertOne({nom: req.body.nom, datecreation: req.body.datecreation, siret: req.body.siret, idexterne: req.body.idexterne, gid: req.body.gid, identifiant: req.body.identifiant}, function(error, result) {
    if(err) return next(err);
    db.collection('gares').findOne({_id : result.insertedId}, function(err, doc) {
      if(err) return next(err);
      res.render('gares', {gare : doc}, function(err, html) {
        if(err) return next(err);
        return res.json({
          response : html
      })
    });
  })
 })
})



});

module.exports = router;
