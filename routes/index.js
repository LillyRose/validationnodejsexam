var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/TodoList', { useNewUrlParser: true }, function (err, client) {
  if (err) throw err

  let db = client.db('examD2D13')
  /* GET home page. */

  router.get('/', function(req, res, next) {
    db.collection('gares').find().sort({position:1}).toArray(function (err, result) {
      res.render('index', { title: 'SNCF', gares: result });

      console.log(result)
    })
  });
})


module.exports = router;
